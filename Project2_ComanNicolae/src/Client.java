import java.util.*;

public class Client {

	
	private int ClientID;
	private Date aTime;
	private long sTime;
	
	public Client(int ClientID, Date aTime, long sTime)
	{
		this.ClientID=ClientID;
		this.aTime=aTime;
		this.sTime=sTime;
		
	}
	
	public int getID()
	{
		return this.ClientID;
	}
	
	public Date getATime()
	{
		return this.aTime;
	}
	
	public long getSTime()
	{
		return this.sTime;
	}
	
}
