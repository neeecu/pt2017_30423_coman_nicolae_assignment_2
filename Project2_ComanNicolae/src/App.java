import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;
public class App {

	static JTextArea ta1 =  new JTextArea  (7,50);
    static	JScrollPane scroll = new JScrollPane(ta1);
    static JTextArea ta2 = new JTextArea (4,50);
   
  
    
	public static  void main(String[] args) {
	 
	 
		
		JFrame frame = new JFrame("Model");  //frame for input and logging
		frame.getContentPane().add(scroll);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,430);
	
		JFrame frame2= new JFrame("Queue Evolution"); //frame for queue evolution 
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame2.setSize(700,200);
		
		
		JPanel p = new JPanel(); // main panel for frame 1
		JPanel pf2= new JPanel(); // main panel for frame 2
		JPanel panel1 = new JPanel(); // panel containint input for min and max arriving time
		
		JPanel panel3 = new JPanel(); //panel containing input for min and max serving time
	
		JPanel panel5 = new JPanel(); // panel containing  input for no_of_queues and running time
		
		JPanel panel7 = new JPanel(); // panel containing the run and clear buttons
		JPanel panel8 = new JPanel(); // panel containing the text area for logging
		JPanel panel9 = new JPanel(); // panel containt text area for auxilary results
		
		
		JLabel l1 = new JLabel("Min Arriving Time  ");
		JLabel l2 = new JLabel("Max Arriving Time ");
		JLabel l3 = new JLabel("Min Serving Time   ");
		JLabel l4 = new JLabel("Max Serving Time ");
		JLabel l5 = new JLabel("Number of Queues");
		JLabel l6 = new JLabel("      Running time    ");
		
		
		
		
		
		JTextField  tf1 = new JTextField (); //for l1
		tf1.setColumns(5);
		
		JTextField  tf2 = new JTextField (); // for l2
		tf2.setColumns(5);
		
		JTextField  tf3 = new JTextField (); // for l3
		tf3.setColumns(5);
		
		JTextField  tf4 = new JTextField (); // for l4
		tf4.setColumns(5);
		
		JTextField  tf5 = new JTextField (); // for l5
		tf5.setColumns(5);
	
		JTextField  tf6 = new JTextField (); // for l6
		tf6.setColumns(5);
		
	 
	
		
		JButton b1 = new JButton("Run");
		JButton b2 = new JButton("Clear");
		
		
		
		panel1.add(l1);
		panel1.add(tf1);
		panel1.setLayout(new FlowLayout());
		
		
		panel1.add(l2);
		panel1.add(tf2);
		panel1.setLayout(new FlowLayout());
		panel1.setPreferredSize(new Dimension(10,2));;
		
		
		panel3.add(l3);
		panel3.add(tf3);
		panel3.setLayout(new FlowLayout());
		
		panel3.add(l4);
		panel3.add(tf4);
		panel3.setLayout(new FlowLayout());
		panel3.setPreferredSize(new Dimension(10,2));

	 
		
		panel5.add(l5);
		panel5.add(tf5);
		panel5.add(l6);
		panel5.add(tf6);
		panel5.setLayout(new FlowLayout());
		panel5.setPreferredSize(new Dimension(10,2));
		
		
	 
		panel7.add(b1);
		panel7.add(b2);
		panel7.setLayout(new FlowLayout());
		panel7.setPreferredSize(new Dimension(10,1));
	
		
		ta1.setEditable(false);
		DefaultCaret caret = (DefaultCaret)ta1.getCaret();
	    caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		panel8.add(scroll);
		panel8.setLayout(new FlowLayout());
		panel8.setSize(7,70);
 		 
		panel9.add(ta2);
		panel9.setLayout(new FlowLayout());
		panel9.setPreferredSize(new Dimension(3,70));
		
		pf2.setLayout(new BoxLayout (pf2, BoxLayout.Y_AXIS));

		 
		p.setLayout(new BoxLayout (p, BoxLayout.Y_AXIS));

		p.add(panel1);
		p.add(panel3);
		p.add(panel5);		
		p.add(panel7);
		p.add(panel8);
		p.add(panel9);

		frame.setContentPane(p);
	    frame.setVisible(true);

	    
	    
	    
	    class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
		      if(e.getSource()==b1){
		    	  
		    	long runningTime=Long.parseLong(tf6.getText());
		  	  	long openQueues=Long.parseLong(tf5.getText());
		  	  	long minAtime=Long.parseLong(tf1.getText());
		  	  	long maxAtime=Long.parseLong(tf2.getText());
		  	  	long minStime=Long.parseLong(tf3.getText());
		  	  	long maxStime=Long.parseLong(tf4.getText()); 
		  	 	
		    	  
		    	
		    	    
		    	  	Model model = new Model(openQueues,runningTime,minAtime,maxAtime,minStime,maxStime);	
		  			for(int i=0;i<openQueues;i++){
		  				model.queues[i].start();
		  				
		  				 
		  				 
		  			}
		  			model.start();
		  			// construct the second frame  and update it as the queues change
		  			
		  			JLabel []labellist  = new JLabel[(int)openQueues];
			    	JTextField [] textlist = new JTextField[(int)openQueues];
			  	  	JPanel [] panellist = new JPanel[(int)openQueues]; 	  
			  	  	frame2.setVisible(true);
			  	  	frame2.setContentPane(pf2);
		  			
		  			for(int i=0;i<openQueues;i++)
			    	  	{

			    	  			JLabel lf2= new JLabel("Q"+i); 
			    	  			JPanel pf1 = new JPanel();
			    	  			JTextField tfa = new JTextField();
			    	  			labellist[i]=lf2;
			    	  			
			    	  			  
			    	  			 
			    	  			model.queues[i].field.setColumns(10);
			    	  			model.queues[i].field.setEditable(false);
			    	  			tfa=model.queues[i].field;
			    	  			textlist[i]=tfa;
			    	  			

			    	  			pf1.add(labellist[i]);
			    	  			pf1.add(textlist[i]);
			    	  			pf1.setLayout(new FlowLayout());
			    	  			panellist[i]=pf1;
			    	  			pf2.add(panellist[i]);
			    	  	}	
		  
		      }
		      if(e.getSource()==b2){

		    	  	tf6.setText(null);
		    	  	tf5.setText(null);
		    	  	tf1.setText(null);
		    	  	tf2.setText(null);
		    	  	tf3.setText(null);
		    	  	tf4.setText(null);
		  
		      }
	    }
	}
	    
	    Handler handler = new Handler();   
	    b1.addActionListener(handler);	
	
	}
}

