import java.util.Date;
import java.util.Vector;

import javax.swing.JTextField;
public class Queue extends Thread  {
	
	
	public JTextField  field = new JTextField (); // update the queue evolution inn a text field for each thread 
												  //  it will be assgned to a text field when the simulation is done
	
	public Vector<Client> Q = new Vector<Client>(); //vector of clients type for each queue
	public  String name ;
	 

	 
	public Queue(){}   // simple constructor

	
	
	public void run(){
		
		
		
					int sec=1000;   // a second variable for sleep periods
					while(Model.runningTime>0){       
						if(Q.isEmpty()==false){
					try {	
						//sleep serving time for each clinet for a accurate simulation
						sleep(Q.elementAt(0).getSTime()*sec);
						
					}
	
					catch(InterruptedException e ){
							e.printStackTrace();
					}
					// update running time after a client is serve

					removeC(); // after it was served remove it

 				 
 				}
			}
			
	}
	public synchronized void addC(Client client){
		Q.add(client);
		// update the textfield text after a client is added to the queue
		field.setText(this.updateQueue());
 		notifyAll();

	}
	
	 
	
	public  synchronized void removeC(){
		try{
		while(Q.isEmpty()==true){
			
			wait();	 // if queue is empty wait until a client is added to it
		}
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
		//copy client into auxilary object in order to generate data
		Client client = Q.elementAt(0);
		//remove the first entry , fifo working style
		Model.runningTime=Model.runningTime-Q.elementAt(0).getSTime();

		Q.removeElementAt(0);

		 //update textfield text after client is removed
		field.setText(this.updateQueue());
		//print both in console and text area 
 		System.out.println("Client "+client.getID()+" was served at "  +" queue "+ this.name +" left at "+ new Date());
		//text area is static, from App class, write directly to it
 		App.ta1.append("\nClient "+client.getID()+" was served at "  +" queue "+ this.name +" left at "+ new Date());
		 

		notifyAll();
	}
	
	
	//construct the current state of the queue
	public synchronized String updateQueue()
	{
		String toReturn="";
		for(int i=0;i<this.Q.size();i++)
		{
			toReturn+=" C" + this.Q.elementAt(i).getID();
		}
		return toReturn;
		
	}
	
	 
	
}	

