import java.util.Date;
import java.lang.Math;
public class Model extends Thread {

	public long totalS=0;  //total serving time
	public long totalA=0;   //total waiting time 
	public long minAtime; //minimum arriving time between clients
	public long maxAtime;  // maximum arriving time betweeen clients
	public long minStime;  //min serving time for a client
	public long maxStime;  // maximum serving time of a clients
	public int no_of_clients=0; 
	public long openQueues;  //no_of_open_queues
	Queue [] queues;        // queues existent in the model
	 static long runningTime; // in seconds
	public long tmpAtime=0; // arriving time for a new client
	public long tmpStime=0;
	 	
	// nr case, running time, min arriving time, max arriving time, min serving time, max serving time
	public Model(long openQueues, long runningTime, long minAtime, long maxAtime, long minStime, long maxStime)
	{
		this.openQueues=openQueues;
		queues=new Queue[(int)openQueues];
		for(int k=0;k<openQueues;k++){
			queues[k]=new Queue();
			queues[k].name= Integer.toString(k);  // the queues are named 0 ,1,2 ...
		}
		Model.runningTime=runningTime;
		this.minAtime=minAtime;
		this.maxAtime=maxAtime;
		this.minStime=minStime;
		this.maxStime=maxStime;
		
		
	}
	
	public void run(){
		
		int sec=1000; //second variable for sleep periods
		while(running()){
			
			//increase number of clients at each step
			no_of_clients+=1;
			
			//generating random arriving time between clients
			tmpAtime = newAtime();
			//create client object
			Client aux = randClient();
			//compute total serving time for each client added to a queue in order to find the averrage waiting time
			totalS+=aux.getSTime();
			//auxiliary queue
			Queue qaux;
			//find the queue with minimum serving time
			long Qtime=queuetime(queues[0]);
			int idx=0;
			for(int k=0;k<openQueues;k++){
				if(queuetime(queues[k])<=Qtime){	
					Qtime=queuetime(queues[k]);
					idx=k;
				}
			}	
		
				qaux =queues[idx]; 
				//add the client to that queue
				System.out.println("Client " +no_of_clients +" added to queue " + idx +" at " +aux.getATime()+" having serving time equal to " + aux.getSTime() );
				App.ta1.append("\nClient " +no_of_clients +" added to queue " + idx +" at " +aux.getATime()+" having serving time equal to " + aux.getSTime() );
				if(qaux.Q.isEmpty()==true)
				{
					runningTime=runningTime-tmpAtime;
					
				}
				qaux.addC(aux);
  			try{
				sleep(tmpAtime*sec); // sleep arriving time for each clinet for a accurate simulation
				
				
			}catch(InterruptedException e){
				e.printStackTrace();
			}


		}

		//update running time
		App.ta2.append("\n Client averrage serving time: "+avStime());
	}
	
	
	
	//returns the index of the minimum serving time queue
	public long queuetime(Queue Q){ 	
		
		long crtSum=0;
			for(int k=0;k<Q.Q.size();k++)
			{
				crtSum=crtSum+Q.Q.elementAt(k).getSTime();
			}
			return   crtSum;
}
 
	public boolean running(){
	  if(runningTime>0)
		  return true;
	  else
		  return false;
	  
 }
	//averrage serving time for a clinet 
	public double avStime()
	{
	
		return ((double)totalS/(double)no_of_clients);
		
	}
	
	//generate new client w/ random serving time 
	public Client randClient()
	{
		long Stime=(long)(Math.random()*(maxStime-minStime+1))+minStime;
		
		
		Client toReturn=new Client(no_of_clients, new Date(),Stime);
		return toReturn;
		
	}
	
	public long newAtime()
	{
		
		return (long)(Math.random()*(maxAtime-minAtime+1))+minAtime;		
	}
	
	
}